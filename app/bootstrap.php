<?php
/**
 * Bootstrap File for including necessary config files and autoloader.
 *
 * @author Mikhail Lantukh <lantukhmikhail@gmail.com>
 */

 $loader = require __DIR__.'/../vendor/autoload.php';
 $loader->add('ApiUtils\\', __DIR__.'/../src/php');

 use Symfony\Component\HttpFoundation\Request;

 $config = (object) include_once(__DIR__.'/config.php');

 $app = new Silex\Application();
 $app['debug'] = $config->debug;

 $app->register(new Silex\Provider\SessionServiceProvider());
 $app->register(new Silex\Provider\TwigServiceProvider(), array(
     'twig.path' => __DIR__.'/../src/php/views',
 ));

 $converter = new ApiUtils\ApiConverter($config);

 $app->get('/api/notam/{icao}', function($icao) use ($app, $converter) {
   return $converter->getNotam($icao);
 });

 $app->get('/', function () use ($app) {
   return $app['twig']->render('index.twig', array());
 });

 $app->get('/notam', function () use ($app) {
   return $app['twig']->render('index.twig', array());
 });

 $app->run();
