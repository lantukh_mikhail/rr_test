/* eslint-disable */
var path = require('path');
var fs = require('fs');
var webpack = require('webpack');
var assign = require('object-assign');

var NODE_ENV = process.env.NODE_ENV || 'development';

var config = {
    context: path.resolve(__dirname, './src/js'),

    entry: {
        app: [path.resolve(__dirname, './src/public/styles/main.scss'), './app.js'],
        vendor: ['react', 'react-dom', 'redux', 'redux-actions', 'react-redux', 'redux-thunk', 'redux-logger',
            'react-router', 'react-router-redux', 'axios', 'react-bootstrap', 'react-google-maps']
    },

    output: {
        path: path.resolve(__dirname, './web'),
        publicPath: '/',
        filename: 'js/[name].js'
    },

    devtool: 'cheap-module-source-map',

    module: {
        loaders: [
            {test: /\.(gif|jpg|png)$/, loader: 'url-loader?limit=10000&mimetype=image/gif'},
            {test: /\.woff(\?v=\d+\.\d+\.\d+)?$/, loader: 'url?limit=10000&mimetype=application/font-woff'},
            {test: /\.woff2(\?v=\d+\.\d+\.\d+)?$/, loader: 'url?limit=10000&mimetype=application/font-woff'},
            {test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/, loader: 'url?limit=10000&mimetype=application/octet-stream'},
            {test: /\.eot(\?v=\d+\.\d+\.\d+)?$/, loader: 'file'},
            {test: /\.svg(\?v=\d+\.\d+\.\d+)?$/, loader: 'url?limit=1000&mimetype=image/svg+xml'},
            {
                test: /\.css$/,
                loader: 'style-loader!css-loader!resolve-url-loader'
            }
        ]
    },

    sassLoader: {
        includePaths: [path.resolve(__dirname, './src/public/styles')]
    },

    resolve: {
        extensions: ['', '.js', '.jsx']
    },

    plugins: [
        new webpack.NoErrorsPlugin(),
        new webpack.optimize.CommonsChunkPlugin('vendor', 'js/vendor.js'),
        new webpack.DefinePlugin({
            'process.env.NODE_ENV': JSON.stringify(NODE_ENV)
        })
    ]
};

module.exports = config;
