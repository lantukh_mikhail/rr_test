/* eslint-disable */
var webpack = require('webpack');
var config = require('./webpack.base.config');
var path = require('path');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var extractSass = new ExtractTextPlugin('css/[name].css');

config.module.loaders.push(
    {
        test: /\.jsx?$/,
        exclude: /node_modules/,
        loaders: ['jsx-loader?harmony', 'babel']
    },
    {
        test: /\.scss$/,
        loader: extractSass.extract('style-loader', 'css-loader!resolve-url-loader!sass-loader?sourceMap')
    }
);

config.sassLoader = {
    includePaths: [path.resolve(__dirname, './src/public/styles')]
};

config.plugins.push(
    extractSass,
    new webpack.optimize.DedupePlugin(),
    new webpack.optimize.UglifyJsPlugin({
        sourceMap: false,
        compress: {
            warnings: false
        }
    }),
    new webpack.optimize.AggressiveMergingPlugin(),
    new webpack.optimize.OccurenceOrderPlugin()
)

module.exports = config;
