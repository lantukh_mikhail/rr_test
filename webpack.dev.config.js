/* eslint-disable */
var webpack = require('webpack');
var config = require('./webpack.base.config');
var path = require('path');
var port = process.env.PORT || '3000';

config.devtool = 'eval';

config.module.preLoaders = [
    {
        test: /\.jsx?$/,
        exclude: /node_modules/,
        loader: 'eslint-loader',
        include: [path.resolve(__dirname, './src/js')]
    }
];

config.module.loaders.push(
    {
        test: /\.jsx?$/,
        exclude: /node_modules/,
        loaders: ['react-hot', 'jsx-loader?harmony', 'babel']
    },
    {
        test: /\.scss$/,
        loader: 'style-loader!css-loader!resolve-url-loader!sass-loader?sourceMap'
    }
);

module.exports = config;
