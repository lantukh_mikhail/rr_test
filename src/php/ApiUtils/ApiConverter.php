<?php
namespace ApiUtils;

/**
 * Class for converting and processing WEB requests to RocketRoute API
 *
 * @author Mikhail Lantukh <lantukhmikhail@gmail.com>
 */
class ApiConverter {
  private $_password;
  private $_username;
  private $_key;
  private $_api;

  /**
   * Instantiate a RocketRoute API Converter.
   *
   * @param array $config API settings
   */
  public function __construct($config) {
      $this->_password = property_exists($config, 'api_pass') ? $config->api_pass : null;
      $this->_username = property_exists($config, 'api_user') ? $config->api_user : null;
      $this->_key = property_exists($config, 'api_key') ? $config->api_key : null;
      $this->_api = property_exists($config, 'api_url') ? $config->api_url : null;
  }

  /**
   * Returns notam list
   *
   * @param string $icao ICAO code
   */
  public function getNotam($icao) {
    $request = '<?xml version="1.0" encoding="UTF-8"?>
      <REQWX>
        <USR>'.$this->_username.'</USR>
        <PASSWD>'.md5($this->_password).'</PASSWD>
        <ICAO>'.$icao.'</ICAO>
      </REQWX>';

    $client = new \Zend\Soap\Client($this->_api.'/notam/v1/service.wsdl');
    $out = new \SimpleXMLElement($client->getNotam($request));

    if ((string) $out->RESULT === '0'){
      return json_encode($out->NOTAMSET);
    } else {
      throw new \Exception($out->RESULT);
    }
  }

/* no need for NOTAM
  private function authenticate() {
    $request = 'req=<?xml version="1.0" encoding="UTF-8" ?>
    <AUTH>
    <USR>lantukhmikhail@gmail.com</USR>
    <PASSWD>'.md5('bZjZH997').'</PASSWD>
    <DEVICEID>1299f2aa8935b9ffabcd4a2cbcd16b8d45691629</DEVICEID>
    <PCATEGORY>RocketRoute</PCATEGORY>
    <APPMD5>GRfXCIq2Hhp4tadCIFVg</APPMD5>
    </AUTH>';
    var_dump($request);

    if( $curl = curl_init() ) {
       curl_setopt($curl, CURLOPT_URL, 'https://flydev.rocketroute.com/remote/auth');
       curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);
       curl_setopt($curl, CURLOPT_POST, true);
       curl_setopt($curl, CURLOPT_POSTFIELDS, $request);
       $out = curl_exec($curl);

       $parsed = new SimpleXMLElement($out);
       $app['session']->set('key', (string) $parsed->KEY);

       var_dump(new SimpleXMLElement($out));
       curl_close($curl);
     }
  }
  */
}
