import React from 'react'
import { render } from 'react-dom'
import { createStore, compose, applyMiddleware } from 'redux'
import createLogger from 'redux-logger'
import thunkMiddleware from 'redux-thunk'
import { Provider } from 'react-redux'
import { Router, browserHistory } from 'react-router'
import { syncHistoryWithStore } from 'react-router-redux'

import reducer from './reducers'
import route from './routes'

const configureStore = (initialState = {}) => {
    const isDev = process.env.NODE_ENV !== 'production'

    const store = createStore(reducer, initialState, compose(
        applyMiddleware(thunkMiddleware),
        isDev ? applyMiddleware(createLogger({collapsed: true})) : (f) => f,
        isDev && window.devToolsExtension ? window.devToolsExtension() : (f) => f
    ))

    if (module.hot) {
        // Enable Webpack hot module replacement for reducers
        module.hot.accept('./reducers', () => {
            const nextRootReducer = require('./reducers/index')
            store.replaceReducer(nextRootReducer)
        })
    }

    return store
}

const selectLocationState = (state) => {
    return state.routing
}

const store = configureStore()

const history = syncHistoryWithStore(browserHistory, store, {selectLocationState})

render(
    <Provider store={store} history={history}>
        <Router history={browserHistory} routes={route}/>
    </Provider>,
    document.getElementById('content')
)

if (module.hot) {
    module.hot.accept()
}
