import { combineReducers } from 'redux'
import notam from './notam'
import routing from './routing'

export default combineReducers({
    notam,
    routing
})
