/*eslint-disable*/
import { handleActions } from 'redux-actions'
import sexagesimal from 'sexagesimal'

export const FETCH = '@@notam/FETCH'
export const START_FETCH = '@@notam/START_FETCH'
export const CHANGE = '@@notam/CHANGE'
export const VALIDATE = '@@notam/VALIDATE'

const initialState = {
    isFetching: false,
    search: null,
    validation: null,
    list: [],
    markers: []
}

export default handleActions({
    [START_FETCH]: (state) => {
        return {...state, isFetching: true}
    },
    [FETCH]: (state, action) => {
        return {...state, list: action.payload, markers: convertNotamToMarkers(action.payload), isFetching: false}
    },
    [CHANGE]: (state, action) => {
        return {...state, search: action.payload, validation: null}
    },
    [VALIDATE]: (state) => {
        return {...state, validation: /^[A-Z]{4}$/.test(state.search) || 'Invalid ICAO code'}
    }
}, initialState)

function convertNotamToMarkers(notam) {
    return notam.map((item, i) => {
        const itemQ = item.ItemQ.split('/')
        const location = itemQ[itemQ.length - 1].replace(/([A-Z])/, '$1 ').split(' ')

        return location.length === 2 ? {
            key: i,
            position: {
                lat: convertLat(location[0]),
                lng: convertLng(location[1])
            },
            description: item.ItemE
        } : null
    }).filter((item) => item !== null)
}

const convertLat = (value) => {
    return sexagesimal(`${value.substr(0, 2)}°${value.substr(2, 2)}′ ${value[value.length - 1]}`)
}

const convertLng = (value) => {
    return sexagesimal(`${value.substr(0, 3)}°${value.substr(3, 2)}′ ${value[value.length - 1]}`)
}
