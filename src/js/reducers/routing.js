import { handleActions } from 'redux-actions'

/**
 * This action type will be dispatched when your history
 * receives a location change.
 */
export const LOCATION_CHANGE = '@@router/LOCATION_CHANGE'

export const initialState = {
    locationBeforeTransitions: null
}

export default handleActions({
    [LOCATION_CHANGE]: (state, action) => {
        return {...state, locationBeforeTransitions: action.payload}
    }
}, initialState)
