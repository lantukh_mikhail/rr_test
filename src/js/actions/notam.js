import { createAction } from 'redux-actions'
import axios from 'axios'
import { CHANGE, FETCH, VALIDATE, START_FETCH } from '../reducers/notam'

export const setNotam = createAction(CHANGE)

export function fetchNotam() {
    return (dispatch, getState) => {
        const validate = createAction(VALIDATE)
        const fetch = createAction(FETCH)
        const start = createAction(START_FETCH)

        return Promise.resolve(dispatch(validate()))
            .then(() => {
                const validation = getState().notam.validation
                return validation === true ? true : Promise.reject(validation)
            })
            .then(() => Promise.resolve(dispatch(start())))
            .then(() => {
                return axios.get(`/api/notam/${getState().notam.search}`)
                    .then((response) => {
                        dispatch(fetch(response.data.NOTAM || []))
                    })
            })
            .catch(() => {
            })
    }
}
