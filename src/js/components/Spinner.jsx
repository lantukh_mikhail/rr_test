import React from 'react'
import classNames from 'classnames'

export default React.createClass({
    displayName: 'Spinner',

    propTypes: {
        visible: React.PropTypes.bool.isRequired
    },

    render() {
        const { children, visible } = this.props

        return (
            <div className={classNames('spinner-wrapper', {'spinner-wrapper--visible': visible})}>
                <div className={classNames('spinner', {hidden: !visible})}>
                    <div className='spinner__body'>
                        <span/>
                        <span/>
                    </div>
                </div>
                {children}
            </div>
        )
    }
})
