import React from 'react'
import { GoogleMapLoader, GoogleMap, Marker, InfoWindow } from 'react-google-maps'

export default React.createClass({
    displayName: 'Map',

    propTypes: {
        markers: React.PropTypes.array
    },

    getInitialState() {
        return {
            markers: this.props.markers || []
        }
    },

    componentWillReceiveProps(props) {
        if (props.markers !== this.props.markers) {
            this.setState({markers: props.markers}, this.zoom)
        }
    },

    zoom() {
        const { markers } = this.state
        if (!markers.length) {
            return
        }
        const bounds = new google.maps.LatLngBounds()
        const map = this.refs.map
        for (let i = 0; i < markers.length; i++) {
            bounds.extend(markers[i].position)
        }

        google.maps.event.addListenerOnce(map, 'bounds_changed', () => {
            this.setZoom(map.getZoom() - 1)

            if (this.getZoom() > 15) {
                this.setZoom(15)
            }
        })

        map.fitBounds(bounds)
    },

    onMarkerClick(targetMarker) {
        return () => {
            this.setState({
                markers: this.state.markers.map((marker) => {
                    return marker === targetMarker ? {...marker, showInfo: true} : marker
                })
            })
        }
    },

    onInfoWindowClose(targetMarker) {
        this.setState({
            markers: this.state.markers.map((marker) => {
                return marker === targetMarker ? {...marker, showInfo: false} : marker
            })
        })
    },

    render() {
        return (
            <section style={{height: '500px', width: '100%'}}>
                <GoogleMapLoader
                    containerElement={<div {...this.props.containerElementProps} style={{height: '100%'}}/>}
                    googleMapElement={
                    <GoogleMap
                        ref='map'
                        defaultZoom={5}
                        defaultCenter={{ lat: -25.363882, lng: 131.044922 }}>
                            {this._renderMarkers()}
                    </GoogleMap>
                    }
                />
            </section>
        )
    },

    _renderMarkers() {
        const image = require('../../public/images/warning-icon-th2.png')
        return this.state.markers.map((marker, index) => {
            const ref = `marker_${index}`
            return (
                <Marker{...marker} ref={ref} key={index} onClick={this.onMarkerClick(marker)} icon={image}>
                    {marker.showInfo ? this._renderInfoWindow(ref, marker) : null}
                </Marker>
            )
        })
    },

    _renderInfoWindow(ref, marker) {
        return (
            <InfoWindow key={`${ref}_info_window`} onCloseclick={() => this.onInfoWindowClose(marker)}>
                <div className='info-block'>
                    {marker.description}
                </div>
            </InfoWindow>
        )
    }
})
