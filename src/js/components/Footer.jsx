import React from 'react'
import { Image, Row, Col } from 'react-bootstrap'

export default function () {
    return (
        <footer className='footer'>
            <Image
                height={50}
                src='http://www.rocketroute.com/wp-content/themes/rocket/_images/content/RocketRoute-Rocket-Large.png'/>
            © lantukhmikhail@gmail.com 2016
        </footer>
    )
}
