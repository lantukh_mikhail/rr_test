import React from 'react'
import { Row, Col, Form, Button, FormControl, FormGroup, ControlLabel, Well, HelpBlock } from 'react-bootstrap'
import { connect } from 'react-redux'
import { setNotam, fetchNotam } from '../actions/notam'
import Map from './Map'
import Spinner from './Spinner'

const mapStateToProps = (state) => {
    return {
        isFetching: state.notam.isFetching,
        value: state.notam.search,
        validation: state.notam.validation,
        markers: state.notam.markers
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        onChange(value) {
            dispatch(setNotam(value))
        },
        onSubmit(e) {
            e.preventDefault()
            dispatch(fetchNotam())
        }
    }
}

const NotamForm = React.createClass({
    displayName: 'NotamForm',
    propTypes: {
        value: React.PropTypes.string,
        validation: React.PropTypes.oneOfType([React.PropTypes.string, React.PropTypes.bool]),
        onChange: React.PropTypes.func,
        onSubmit: React.PropTypes.func
    },

    handleChange(e) {
        const onChange = this.props.onChange
        if (typeof onChange === 'function') {
            onChange(e.target.value)
        }
    },

    render() {
        const { markers, value, onSubmit, validation, isFetching } = this.props
        const hasError = typeof validation === 'string'
        const inputProps = {}
        if (hasError) {
            inputProps.validationState = 'error'
        }

        return (
            <Spinner visible={isFetching}>
                <Row>
                    <Col xs={8}>
                        <Map markers={markers || []}/>
                    </Col>
                    <Col xs={4}>
                        <Well>
                            <Form onSubmit={onSubmit} className='clearfix'>
                                <FormGroup controlId='icao' {...inputProps}>
                                    <ControlLabel>Enter ICAO code for getting NOTAM</ControlLabel>
                                    <FormControl type='text' value={value || ''}
                                                 onChange={this.handleChange}/>
                                    <HelpBlock>{hasError ? validation : null}</HelpBlock>
                                </FormGroup>

                                <Button bsStyle='success' onClick={onSubmit} className='pull-right'>Submit</Button>
                            </Form>
                        </Well>
                    </Col>
                </Row>
            </Spinner>
        )
    }
})

export default connect(mapStateToProps, mapDispatchToProps)(NotamForm)
