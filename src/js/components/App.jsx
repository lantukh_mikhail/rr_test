import React from 'react'
import { Grid } from 'react-bootstrap'
import Header from './Header'
import Footer from './Footer'

export default function (props) {
    return (
        <div>
            <Header />
            <Grid>
                {props.children}
            </Grid>
            <Footer />
        </div>
    )
}
