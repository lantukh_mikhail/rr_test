import React from 'react'
import { Navbar, Image } from 'react-bootstrap'
import { Link } from 'react-router'

export default function () {
    const logo = require('../../public/images/RocketRoute-Logo.svg')
    return (
        <Navbar>
            <Navbar.Header>
                <Navbar.Brand>
                    <Link to='/'>
                        <Image src={logo} /><span>Mikhail Lantukh's Test Application</span>
                    </Link>
                </Navbar.Brand>
            </Navbar.Header>
        </Navbar>
    )
}
