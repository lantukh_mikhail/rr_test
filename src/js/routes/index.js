import App from '../components/App'
import NotamForm from '../components/NotamForm'

export default {
    path: '/',
    component: App,
    indexRoute: {onEnter: (nextState, replace) => replace('/notam')},
    childRoutes: [
        {path: '/notam', component: NotamForm}
    ]
}
