# Getting started

   - Clone repository (git clone <repo>)    
```sh
$node -v #must be 4.*
$npm -v #must be 3.*
$git clone <repo>
```

   - Install dependencies   
```sh
$php composer.phar install # composer.phar has to be on the root or installed globally
$npm i --no-optional (for not OsX)
```

## Build modes

   - Development mode
```sh
$npm start
```  

   - Production build  
```sh
$npm run build
```    
