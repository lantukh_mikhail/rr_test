/* eslint-disable */
var webpack = require('webpack');
var webpackDevServer = require('webpack-dev-server');
var config = require('./webpack.dev.config.js');

var PORT = 3000;

config.entry.app.unshift('webpack-dev-server/client?http://localhost:' + PORT, 'webpack/hot/dev-server');
config.plugins.push(new webpack.HotModuleReplacementPlugin());

var server = new webpackDevServer(webpack(config), {
    contentBase: 'web/',

    hot: true,

    publicPath: '/',

    stats: {
        colors: true
    },

    watchOptions: {
        aggregateTimeout: 100
    },

    historyApiFallback: true,

    proxy: {
        '/api/*': {
           target: 'http://192.168.56.101/',
           ws: true
        }
    }
});


console.log('Starting build in development mode...');

server.listen(PORT);
